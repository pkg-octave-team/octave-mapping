Source: octave-mapping
Section: math
Priority: optional
Maintainer: Debian Octave Group <team+pkg-octave-team@tracker.debian.org>
Uploaders: Rafael Laboissière <rafael@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-octave (>= 1.2.3),
               dh-sequence-octave,
               libgdal-dev,
               octave-io (>= 2.6.1-2),
               pkgconf
Standards-Version: 4.7.2
Homepage: https://gnu-octave.github.io/packages/mapping/
Vcs-Git: https://salsa.debian.org/pkg-octave-team/octave-mapping.git
Vcs-Browser: https://salsa.debian.org/pkg-octave-team/octave-mapping
Testsuite: autopkgtest-pkg-octave
Rules-Requires-Root: no

Package: octave-mapping
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, ${octave:Depends}
Description: geographical mapping functions for Octave
 This package contains simple mapping and GIS .shp and raster file
 functions for Octave, a numerical computation software.  It includes
 functions for computing angular distances and displacements between
 points on a sphere, as well as the great circle azimuth.
 .
 This Octave add-on package is part of the Octave-Forge project.
