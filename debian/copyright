Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Mapping package for Octave
Upstream-Contact: The Octave Community <octave-maintainers@octave.org>
Source: https://gnu-octave.github.io/packages/mapping/

Files: *
Copyright: 2004-2022 Andrew Collier <abcollier@users.sourceforge.net>
           2006, 2008 Junio C Hamano
           2006-2022 Alexander Barth <abarth93@users.sourceforge.net>
           2013-2022 Carnë Draug <carandraug@octave.org>
           2014-2022 Alfredo Foltran <alfoltran@gmail.com>
           2014-2022 Philip Nienhuis <prnienhuis@users.sf.net>
           2014-2022 Eugenio Gianniti <eugenio.gianniti@mail.polimi.it>
           2014-2022 Pooja Rao <poojarao12@gmail.com>
           2015-2022 Markus Bergholz <markuman@gmail.com>
           2015-2022 Oscar Monerris Belda
           2015-2022 Shashank Khare <skhare@hotmail.com>
           2013-2022 Felipe Geremia Nievinski
           2014-2022 Michael Hirsch
           2018-2022 Ricardo Fantin da Costa <ricardofantin@gmail.com>
License: GPL-3+

Files: debian/*
Copyright: 2008 Olafur Jens Sigurdsson <ojsbug@gmail.com>
           2012 Thomas Weber <tweber@debian.org>
           2016-2022 Rafael Laboissière <rafael@debian.org>
License: GPL-3+

License: GPL-3+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General Public
 License, version 3, can be found in the file
 `/usr/share/common-licenses/GPL-3'.
